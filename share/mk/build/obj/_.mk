# Copyright 2022-2024, Alejandro Colomar <alx@kernel.org>
# SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception


ifndef MAKEFILE_BUILD_OBJ_INCLUDED
MAKEFILE_BUILD_OBJ_INCLUDED := 1


.PHONY: build-obj
build-obj: build-obj-cpp build-obj-pch build-obj-cc build-obj-as;


endif  # include guard
