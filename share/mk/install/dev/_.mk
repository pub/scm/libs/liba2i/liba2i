# Copyright 2022-2024, Alejandro Colomar <alx@kernel.org>
# SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception


ifndef MAKEFILE_INSTALL_DEV_INCLUDED
MAKEFILE_INSTALL_DEV_INCLUDED := 1


.PHONY: install-dev
install-dev: install-dev-include install-dev-pc;


endif  # include guard
