#!/usr/bin/env bash
# Copyright 2024 Alejandro Colomar <alx@kernel.org>
# SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception


set -Eeuf;


out="$(mktemp)";


$CC $CFLAGS_ -o "$out" -x c - $LDLIBS_ 2>&1 <<__EOF__ \
| if ! grep -- 'incompatible-pointer-types' >/dev/null; then \
	>&2 printf '%s\n' "$0:$LINENO: Expected: [incompatible-pointer-types]"; \
	exit 1; \
else \
	true; \
fi;
	#include <a2i/a2i/a2i.h>

	int
	main(void)
	{
		char   *end;
		short  n;

		a2i(short, &n, "0", &end, 0, 0, 0);
	}
__EOF__


$CC $CFLAGS_ -o "$out" -x c - $LDLIBS_ 2>&1 <<__EOF__ \
| if ! grep -- 'pointer-sign' >/dev/null; then \
	>&2 printf '%s\n' "$0:$LINENO: Expected error: [pointer-sign]"; \
	exit 1; \
else \
	true; \
fi;
	#include <stddef.h>
	#include <a2i/a2i/a2i.h>

	int
	main(void)
	{
		unsigned char  n;

		a2i(signed char, &n, "0", NULL, 0, 0, 0);
	}
__EOF__


$CC $CFLAGS_ -o "$out" -x c - $LDLIBS_ 2>&1 <<__EOF__ \
| if ! grep -- 'incompatible-pointer-types' >/dev/null; then \
	>&2 printf '%s\n' "$0:$LINENO: Expected error: [incompatible-pointer-types]"; \
	exit 1; \
else \
	true; \
fi;
	#include <stddef.h>
	#include <a2i/a2i/a2i.h>

	int
	main(void)
	{
		unsigned long  n;

		a2i(unsigned int, &n, "0", NULL, 0, 0, 0);
	}
__EOF__


$CC $CFLAGS_ -o "$out" -x c - $LDLIBS_ 2>&1 <<__EOF__ \
| if ! grep -- 'error:.*[Gg]eneric.* association' >/dev/null; then \
	>&2 printf '%s\n' "$0:$LINENO: Expected error: [_Generic]"; \
	exit 1; \
else \
	true; \
fi;
	#include <stddef.h>
	#include <a2i/a2i/a2i.h>

	int
	main(void)
	{
		float  n;

		a2i(float, &n, "0", NULL, 0, 0, 0);
	}
__EOF__
